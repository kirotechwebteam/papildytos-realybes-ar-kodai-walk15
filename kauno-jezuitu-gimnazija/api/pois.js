export default [
    {
        id: "y1",
        name :"Jėzuitu gimnazijos įėjimas",
        nickname: "school",
        location: {
            lat: 54.895799,
            lon: 23.885279,
        },
        image: "https://lh5.googleusercontent.com/p/AF1QipM7zf8Hd8cWH8g-8OPxFOGM_Ieue1PVhr5iCbSi=w408-h306-k-no",
    },
    {
        id: "y2",
        name :"",
        nickname: "street1",
        location: {
            lat: 54.895559,
            lon: 23.885075,
        },
        image: "https://interjero-linija.lt/wp-content/uploads/2015/12/905_resized1.jpg",
    },
    {
        id: "y3",
        name :"",
        nickname: "street3",
        location: {
            lat: 54.995559,
            lon: 23.785075,
        }
    },
    {
        id: "y4",
        name :"",
        nickname: "street4",
        location: {
            lat: 54.795559,
            lon: 23.985075,
        }
    },
]